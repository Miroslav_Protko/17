/*Author Miroslav Protko*/
.model small
.stack 100h
.data
	a dw 0
	b dw 0
	c dw 0
	d dw 0
	errorstr db 13,"Incorrect input$"

.code
start:

	mov ax, @data
	mov ds, ax

	call Main

	mov ah, 4Ch
	int 21h

Main proc

	push ax
	push cx
	push bx
	push dx
	;ввод значений
	call Read ;
	mov a, ax
	call Read ;
	mov b, ax
	call Read ;
	mov c,ax
	call Read ;
	mov d,ax

;Выполнение основного условия:

MOV BX, a ;move a to bx
XOR BX, b ;a xor b

MOV AX, c ;move c to ax
MOV CX, c ;move c to cx
MUL CX ;ax *= cx
MUL CX ;ax *= d
MUL CX ;ax *= d
MUL CX ;ax *= d

CMP AX, BX  ;compare bx and ax
JE F1 ;if bx = ax goto: f1

MOV AX,a
MOV DX, b
OR DX,c
SUB AX,DX
JMP FINISH


F2:
 MOV BX,a
 MOV AX,c
 MOV DX, d
 MUL DX
 ADD AX,BX
 JMP FINISH



F1:
 MOV BX, a
 ADD BX, b
 MOV AX, c
 MOV DX, d
 MUL  DX
 CMP AX,BX
 JE F2
 MOV AX,a
 JMP FINISH
 FINISH:

  call show; вывод на экран

	pop ax
	pop bx
	pop dx
	pop cx
	ret

Main endp


show proc

	push ax
	push bx
	push cx
	push dx

	xor cx,cx
	mov bx, 10

 	loop1:

		xor dx,dx
		div bx
		add dl, '0' ; получаем код символа
		push dx ; сохраняем последнюю цифру
		inc cx ; увеличиваем количество цифр в стеке
		cmp ax, 0 ; if true zf=1
		jnz loop1 ;if zf=1 then continue

;cx times
	loop2:
		pop dx
		mov ah, 02h
		int 21h
	  loop loop2

		pop dx
		pop cx
		pop bx
		pop ax
	ret
show endp


Read proc
	push dx
	push bx
	push cx

  xor cx,cx

    startloop:

		mov ah, 01h ;ввод символа, сохранение в al
		int 21h

		cmp al, 13 ; если ввод закончен, нажата клавиша Enter
		je returnvalue

		cmp  al, 27    ; если ESC - на выход
    je   exit
		cmp  al, 8    ; если backspace
    je   back

		jmp checkvalue
		jmp startloop


		back:

						mov  ah,03h        ; определяем текущее
						int  10h           ; положение курсора

						push dx            ; сохраняем эту позицию
						mov  ah,02h        ; удаляем  символ
						mov  dl,''
						int  21h
						pop  dx
						mov  ah,02h        ; возвращаем курсор
						int  10h

		        jmp  startloop

	checkvalue:

		; проверка на не цифру
		cmp al, '0'
		jl errormessage
		cmp al, '9'
		jg errormessage

		push ax
		; проверка на размер
		mov ax, cx
		mov bx, 10
		mul bx
		mov cx, ax
		cmp dx, 0
		jnz errormessage ; если превысило размер

		pop ax

		; получаем само число
		mov ah, 0
		sub al, '0'
		add cx, ax
		jc errormessage ; если превысило размер

		jmp startloop

	returnvalue:

		mov ax, cx

		pop cx
		pop bx
		pop dx

    ret

Read endp

errormessage:
	pop cx
	pop bx
	pop dx
	lea dx, errorstr ;  адрес -> dx    *offset*
	mov ah, 09h
	int 21h
	mov ah, 4Ch
	int 21h
exit:
					pop cx
					pop bx
					pop dx
					mov ah, 4Ch
					int 21h
end start
